import Layout from '../../components/layout'
import Head from 'next/head'
import Image from 'next/image'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip'
import { getAllPostIds, getPostData } from '../../lib/posts'
import Date from '../../components/date'
import { Container } from '@mui/material'
import PropTypes from 'prop-types';

function Item(props) {
    const { sx, ...other } = props;
    return (
      <Box
        sx={{
          bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : '#fff'),
          color: (theme) => (theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800'),
          border: '1px solid',
          borderColor: (theme) =>
            theme.palette.mode === 'dark' ? 'grey.800' : 'grey.300',
          p: 1,
          borderRadius: 2,
          fontSize: '0.875rem',
          fontWeight: '700',
          ...sx,
        }}
        {...other}
      />
    );
  }
  
  Item.propTypes = {
    sx: PropTypes.oneOfType([
      PropTypes.arrayOf(
        PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.bool]),
      ),
      PropTypes.func,
      PropTypes.object,
    ]),
  };

export async function getStaticProps({ params }) {
    const postData = await getPostData(params.id)
    return {
        props: {
            postData
        }
    }
}

export async function getStaticPaths() {
    const paths = getAllPostIds()
    return {
        paths,
        fallback: false
    }
}

export default function Post({ postData }) {
    return (
        <Layout>
            
            <Head>
                <title>{postData.title}</title>
            </Head>

            <Container component="main" sx={{ flexGrow: 1, mt: 0, mb: 2}}>
                <Image
                    priority
                    src={"/" + postData.graphic}
                    height={1365}
                    width={2048}
                    layout='intrinsic'
                    alt={postData.title}
                />

                <Box
                    sx={{
                        display: 'grid',
                        gridAutoColumns: '1fr',
                        gap: 1,
                    }}
                >
                    <Item sx={{ gridRow: '1', gridColumn: 'span 2' }}>span 2</Item>
                    {/* The second non-visible column has width of 1/4 */}
                    <Item sx={{ gridRow: '1', gridColumn: '4 / 5' }}>4 / 5</Item>
                </Box>

                <Typography sx={{ textAlign: 'center' }} variant="h5">
                    {postData.title}
                </Typography>
                <Typography sx={{ textAlign: 'right' }} variant="h5">
                    {postData.title}
                </Typography>

                <Divider>
                    <Chip label={postData.date} />
                </Divider>

                <Typography variant="body2" gutterBottom>
                    <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
                </Typography>
            </Container>
        </Layout>
    )
}


