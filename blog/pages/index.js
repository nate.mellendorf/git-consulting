import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import { getSortedPostsData } from '../lib/posts'
import Link from 'next/link'
import Date from '../components/date'
import Card from '@mui/material/Card';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, CardActions } from '@mui/material';
import ShareIcon from '@mui/icons-material/Share';
import IconButton from '@mui/material/IconButton';

import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({

}));

function GridItem({ classes, id, date, title, brief, graphic }) {
  return (
    // From 0 to 600px wide (smart-phones), I take up 12 columns, or the whole device width!
    // From 600-690px wide (tablets), I take up 6 out of 12 columns, so 2 columns fit the screen.
    // From 960px wide and above, I take up 25% of the device (3/12), so 4 columns fit the screen.
    <Grid item xs={12} sm={6} md={4}>
      <Card sx={{ maxWidth: 500 }}>
        <Link href={`/posts/${id}`}>
          <CardActionArea>
            <CardMedia
              component="img"
              height="200"
              image={graphic}
              alt="green iguana"
            />
            <CardContent>
              <Typography gutterBottom variant="h6" component="div">
                {title}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                {brief}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Link>
        <CardActions>
          <IconButton aria-label="share">
            <ShareIcon />
          </IconButton>
          <Typography sx={{ textAlign: 'right' }} variant="body2" color="text.secondary">
            <Date dateString={date} />
          </Typography>
        </CardActions>
      </Card>
    </Grid>
  );
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function Home({ allPostsData }) {
  const classes = useStyles()
  return (
    <Layout>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <Grid container spacing={2}>
        {allPostsData.map(({ id, date, title, brief, graphic }) => (
          <GridItem item
            classes={classes}
            id={id}
            date={date}
            title={title}
            brief={brief}
            graphic={graphic}>
          </GridItem>
        ))}
      </Grid>
    </Layout>
  )
}
