import '../styles/global.css'
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import { useEffect } from "react";

const darkTheme = createTheme({
    palette: {
        mode: 'dark',
    },
    components: {
        // Name of the component
        Link: {
            styleOverrides: {
                // Name of the slot
                root: {
                    // Some CSS
                    textDecoration: 'none',
                },
            },
        },
    }
});

export default function App({ Component, pageProps }) {

    useEffect(() => {
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
    }, []);

    return (
        <ThemeProvider theme={darkTheme}>
            <CssBaseline />
            <Component {...pageProps} />
        </ThemeProvider>)
}
