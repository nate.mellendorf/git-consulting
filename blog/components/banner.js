import Image from 'next/image'
import Box from '@mui/material/Box';
import Container from '@mui/material/Box';
const Banner = () => {
    return (
        <Container sx={{ height: '25%' }}>
            <Image
                priority
                src={"/images/banner.jpg"}
                layout='responsive'

                height={960}
                width={1280}
                alt="banner"
            />
        </Container>
    );
}

export default Banner