import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
import CopyrightIcon from '@mui/icons-material/Copyright';
import { Container } from '@mui/material';

const Footer = () => {
    return (
        <Container component="main" sx={{ pt: 2, pb: 2}}>
            <Divider>
                <Chip icon={<CopyrightIcon />} label="Copyright 2022" />
            </Divider>
        </Container>
    );
}

export default Footer